job "three-instances" {
  datacenters = ["dc1"]

  group "service" {
    count = 3

    task "app" {
      driver = "docker"

      config {
        image = "nginx:latest"
        ports = ["http"]
      }

      resources {
        cpu    = 50 # в милливаттах
        memory = 10 # в мегабайтах
        network {
          mbits = 10
          port "http" {
             to = 8088
          }
        }
      }
    }
  }
}
