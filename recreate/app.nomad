job "web-server" {
  datacenters = ["dc1"]

  group "web" {
    count = 1

    task "web-server" {
      driver = "docker"

      config {
        image = "nginx:latest"
        port_map = {
          http = 80
        }
      }

      resources {
        cpu    = 500 # in MHz
        memory = 256 # in MB
        network {
          mbits = 10
          port "http" {}
        }
      }
    }
  }
}
