project = "first-app"

app "first-app" {
  build {
    use "docker" {
      buildkit = false
      disable_entrypoint = false
    }
    registry {
      use "docker" {
        image = "app"
        tag = "1"
        local = true
      }
    }
  }


  deploy {
    use "nomad-jobspec" {
      jobspec = templatefile("${path.app}/first-app.nomad.tpl")
    }
  }


  release {
    use "nomad-jobspec-canary" {
      groups = [
        "app"
      ]
    }
  }
}
